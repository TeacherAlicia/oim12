// Server listens for socket.io communication at port 8000
var io = require('socket.io').listen(8000); 

io.sockets.on('connection', function (socket) {
  console.log('connected');

  // If socket.io receives message from the client browser then 
    // this call back will be executed.
    socket.on('message', function (msg) {
      console.log(msg);
    });

    // If a web browser disconnects from Socket.IO then this callback is called.
    socket.on('disconnect', function () {
      console.log('disconnected');
    });
});

var five = require("johnny-five");
var board = new five.Board();

board.on("ready", function() {
  var proximity = new five.Proximity({
    controller: "HCSR04",
    pin: 7
  });

  proximity.on("data", function() {
    console.log("  proximity in cm  : ", this.cm);

    io.sockets.emit('proximity', this.cm); 
  });
});
