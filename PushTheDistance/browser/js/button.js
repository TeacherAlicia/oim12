$(document).ready(function()
{
    var socket = io('http://localhost:8000');
    var aantal = 0;

    var videoStatus = false;
    var videoOldStatus = false;
    var pushIt = '<video width="100%" height="100%" controls autoplay><source src="video/pushIt.mp4" type="video/mp4">Your browser does not support the video tag.</video>';

    socket.on('connect', function () {
        socket.on('button', function (msg) {
            if (msg == "down") {
            	$("body").css("background", "black");
            	videoStatus = false;
            	aantal++;
            }
            else if (msg == "hold") {
            	$("body").css("background", "green");
            	videoStatus = true;
            }
            else if (msg == "up") {
            	$("body").css("background", "pink");
            	videoStatus = false;
            }
            else {
            	$("body").css("background", "yellow");
            	videoStatus = false;
            }

            if (videoStatus == true && videoOldStatus == false) {
            	$("article#video").html(pushIt);
            }
            else if (videoStatus == false)
            {
            	$("article#video").html("");
            }
            videoOldStatus = videoStatus;

            $("#status").html(msg);
            $("#aantal").html(aantal);
        });
    });
});