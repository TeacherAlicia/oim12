$(document).ready(function()
{
    var socket = io('http://localhost:8000');

    socket.on('connect', function () {
        socket.on('proximity', function (val) {
            $("#afstand").html(val);


            // Convert the value to a color
            var color = (val * 5) - 20; 
            color = color < 0 ? 0 : color > 255 ? 255 : color;

            // Update background color of the body
            $("body").css("background-color", "rgb(" + color + "," + color + "," + color + ")" );
        });
    });
});